@if(count($errors)>0)
 	@foreach($errors->all() as $error)
 		<li><?php echo $error ?></li>
 	@endforeach	
@endif
 	@extends('layouts.admin')
	@section('content')
	{!!Form::open(['route'=>'usuario.store', 'method'=>'POST'])!!}
	
	@include('usuario.form.usr')
	
	{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
	{!!Form::close()!!}
	@endsection